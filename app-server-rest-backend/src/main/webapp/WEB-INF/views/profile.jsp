<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h1>CURRENT PROFILE</h1>

<form:form modelAttribute="user" method="get">

    <p>
    <div>ID:
            <form:input path="id" size="36" disabled="true"/>
    </p>

    <p>
    <div>LOGIN:
            <form:input path="login" size="15" disabled="true"/>
    </p>

    <p>
    <div>FIRST NAME: <form:input path="firstName" size="15" disabled="true"/></div>
    </p>

    <p>
    <div>SECOND NAME: <form:input path="secondName" size="15" disabled="true"/></div>
    </p>

    <p>
    <div>LAST NAME: <form:input path="lastName" size="15" disabled="true"/></div>
    </p>

    <p>
    <div>PASSWORD HASH: <form:input path="passwordHash" size="71" disabled="true"/></div>
    </p>

</form:form>

<br>

<form action="/profile/edit">
    <button>
        EDIT
    </button>
</form>

<jsp:include page="../include/_footer.jsp"/>