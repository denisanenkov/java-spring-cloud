<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT LIST</h1>
<h1>YOUR LOGIN: ${name}</h1>

<table width="100%" border="1" cellpadding="10" style="border-collapse: collapse;">

    <tr>
        <th width="100" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%">DESCRIPTION</th>
        <th width="170" nowrap="nowrap" align="center">STATUS</th>
        <th width="170" nowrap="nowrap" align="center">DATE BEGIN</th>
        <th width="170" nowrap="nowrap" align="center">DATE FINISH</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center">
                <c:out value="${project.status.displayName}"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.dateBegin}" type="date" pattern="dd-MM-yyyy"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.dateFinish}" type="date" pattern="dd-MM-yyyy"/>
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>

</table>

<br>
<br>

<form action="/project/create">
    <button>
        CREATE PROJECT
    </button>
</form>

<br>

<form action="/">
    <button>
        START PAGE
    </button>
</form>

<jsp:include page="../include/_footer.jsp"/>