<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<form:form action="/profile/edit/save" method="post" modelAttribute="user">

    <h1>CURRENT PROFILE</h1>

    <p>
    <div>ID:
            <form:input path="id" size="35" disabled="true"/>
    </p>

    <p>
    <div>LOGIN:
            <form:input path="login" size="15" disabled="true"/>
    </p>

    <p>
    <div>FIRST NAME: <form:input path="firstName" size="15"/></div>
    </p>

    <p>
    <div>SECOND NAME: <form:input path="secondName" size="15"/></div>
    </p>

    <p>
    <div>LAST NAME: <form:input path="lastName" size="15"/></div>
    </p>

    <p>
    <div>PASSWORD HASH: <form:input path="passwordHash" size="70" disabled="true"/></div>
    </p>

    <br>

    <button type="submit">SAVE</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>