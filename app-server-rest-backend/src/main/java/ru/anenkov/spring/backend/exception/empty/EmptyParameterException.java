package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyParameterException extends AbstractException {

    public EmptyParameterException() {
        System.out.println("Error! Empty name parameter exception..");
    }

}
