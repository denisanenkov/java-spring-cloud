package ru.anenkov.spring.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.anenkov.spring.backend.model.User;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String>, JpaRepository<User, String> {

    @Nullable
    @Transactional(readOnly = true)
    User findByLogin(@Nullable @Param("login") String login);

    @Transactional
    void deleteUserByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByLastName(@Nullable @Param("last_name") String last_name);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByFirstName(@Nullable @Param("first_name") String first_name);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllBySecondName(@Nullable @Param("second_name") String second_name);

}
