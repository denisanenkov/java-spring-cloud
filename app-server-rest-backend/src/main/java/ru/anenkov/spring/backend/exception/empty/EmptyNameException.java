package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty!...");
    }

}