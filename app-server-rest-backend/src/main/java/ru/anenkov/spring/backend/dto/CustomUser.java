package ru.anenkov.spring.backend.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Setter
@Getter
public class CustomUser extends User {

    private String userId;

    public CustomUser withUserId(@NotNull final String userId) {
        this.userId = userId;
        return this;
    }

    public CustomUser(
            UserDetails user
    ) {
        super(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities());
    }

    public CustomUser(@NotNull final String username,
                      @NotNull final String password,
                      @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password, authorities);
    }

    public CustomUser(@NotNull final String username,
                      @NotNull final String password,
                      boolean enabled,
                      boolean accountNonExpired,
                      boolean credentialsNonExpired,
                      boolean accountNonLocked,
                      @NotNull final Collection<? extends GrantedAuthority> authorities
    ) {
        super(username, password,
                enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);
    }

}
