package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error! User is Empty..");
    }
}
