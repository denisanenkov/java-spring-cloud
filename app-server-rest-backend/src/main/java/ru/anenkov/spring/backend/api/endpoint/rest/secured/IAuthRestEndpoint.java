package ru.anenkov.spring.backend.api.endpoint.rest.secured;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.dto.UserDTO;
import ru.anenkov.spring.backend.model.User;

import java.util.List;

public interface IAuthRestEndpoint {

    @Nullable
    @SneakyThrows
    @GetMapping("/users")
    List<UserDTO> findAll();

    @SneakyThrows
    @DeleteMapping("/users")
    void deleteAll();

    @SneakyThrows
    @GetMapping("/users/count")
    long count();

    @SneakyThrows
    @GetMapping(value = "/logout")
    void logout();

    @SneakyThrows
    @GetMapping(value = "/profile")
    User profile();

    @Nullable
    @SneakyThrows
    @PostMapping("/users")
    User save(@RequestBody @Nullable User user);

    @SneakyThrows
    @DeleteMapping("/users/byBody")
    void delete(@RequestBody @Nullable User user);

    @Nullable
    @SneakyThrows
    @PutMapping("/users")
    User update(@RequestBody @Nullable User user);

    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    UserDTO findById(@PathVariable @Nullable String id);

    @SneakyThrows
    @DeleteMapping("/users/{id}")
    void deleteById(@PathVariable @Nullable String id);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/login/{login}")
    UserDTO findByLogin(@PathVariable @Nullable String login);

    @SneakyThrows
    @DeleteMapping("/users/login/{login}")
    void deleteUserByLogin(@PathVariable @Nullable String login);

    @SneakyThrows
    @PutMapping("/users/updateFirstName/{newFirstName}")
    void updateFirstName(@PathVariable @Nullable String newFirstName);

    @SneakyThrows
    @PutMapping("/users/updateSecondName/{newSecondName}")
    void updateSecondName(@PathVariable @Nullable String newSecondName);

    @SneakyThrows
    @PutMapping("/users/updateLastName/{newLastname}")
    void updateLastName(@PathVariable @Nullable String newLastname);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/lastName/{lastName}")
    UserDTO findUserByLastName(@PathVariable @Nullable String lastName);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/firstName/{firstName}")
    UserDTO findUserByFirstName(@PathVariable @Nullable String firstName);

    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/secondName/{secondName}")
    UserDTO findUserBySecondName(@PathVariable @Nullable String secondName);

    @SneakyThrows
    @GetMapping(value = "/auth/{username}/{password}")
    boolean login(@PathVariable @Nullable String username, @PathVariable @Nullable String password);

}
