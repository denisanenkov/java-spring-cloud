package ru.anenkov.spring.backend.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.spring.backend.enumerated.Status;
import ru.anenkov.spring.backend.model.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO extends AbstractEntityDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "name")
    private String name = "";

    @Column(name = "description")
    private String description = "";

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "project_id")
    private String projectId;

    @Column(name = "user_id")
    private String userId;

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final String projectId,
            @NotNull final String userId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "[TASK" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", NAME = '" + name + "';\n" +
                ", DESCRIPTION = '" + description + "';\n" +
                ", STATUS = " + status + "';\n" +
                ", DATE BEGIN = " + dateBegin + "';\n" +
                ", DATE FINISH = " + dateFinish + "';]\n" +
                ", PROJECT ID = " + projectId + "';\n" +
                ", USER ID = " + userId + "';\n";
    }

    @NotNull
    public static TaskDTO toTaskDTO(@NotNull final Task task) {
        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setDateBegin(task.getDateBegin());
        taskDTO.setDateFinish(task.getDateFinish());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        if (task.getUser() != null) taskDTO.setUserId(task.getUser().getId());
        return taskDTO;
    }

    @NotNull
    public static List<TaskDTO> toTaskListDTO(@NotNull final List<Task> taskList) {
        @Nullable List<TaskDTO> taskDTOList = new ArrayList<>();
        for (@Nullable Task task : taskList) {
            taskDTOList.add(toTaskDTO(task));
        }
        return taskDTOList;
    }

}
