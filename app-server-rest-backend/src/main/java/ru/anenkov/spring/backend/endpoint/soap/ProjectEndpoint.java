package ru.anenkov.spring.backend.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.api.endpoint.soap.IProjectEndpoint;
import ru.anenkov.spring.backend.dto.ProjectDTO;
import ru.anenkov.spring.backend.service.ProjectService;
import ru.anenkov.spring.backend.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

/**
 * address: http://localhost:8282/services/ProjectEndpoint?wsdl
 * 2th address: http://localhost:8383/services/ProjectEndpoint?wsdl
 */

@Component
@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "project") ProjectDTO project
    ) {
        projectService.addDTO(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneByIdEntity(
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(Objects.requireNonNull(projectService.findProjectByIdAndUserId(UserUtil.getUserId(), id)));
    }

    @Override
    @WebMethod
    public ProjectDTO findProjectByUserIdAndId(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(Objects.requireNonNull(projectService.findProjectByIdAndUserId(userId, id)));
    }

    @Override
    @WebMethod
    public void removeOneById(
            @WebParam(name = "id") @Nullable final String id
    ) {
        projectService.removeProjectByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getList() {
        return ProjectDTO.toProjectListDTO(Objects.requireNonNull(projectService.findAllByUserId(UserUtil.getUserId())));
    }

    @Override
    @WebMethod
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    public ProjectDTO findProjectByUserIdAndName(
            @Nullable @WebParam(name = "name") final String name
    ) {
        return projectService.toProjectDTO(projectService.findProjectByUserIdAndName(UserUtil.getUserId(), name));
    }

}
