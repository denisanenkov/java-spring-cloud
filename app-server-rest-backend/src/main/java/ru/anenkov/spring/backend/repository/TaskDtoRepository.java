package ru.anenkov.spring.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.spring.backend.dto.TaskDTO;

public interface TaskDtoRepository extends JpaRepository<TaskDTO, String> {
}
