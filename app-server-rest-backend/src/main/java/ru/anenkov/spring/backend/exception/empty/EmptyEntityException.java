package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyEntityException extends AbstractException {

    public EmptyEntityException() {
        super("Error! Entity is Null..");
    }

}
