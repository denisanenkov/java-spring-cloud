package ru.anenkov.spring.backend.endpoint.rest.unsecured;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.api.endpoint.rest.unsecured.IAuthRestFreeEndpoint;
import ru.anenkov.spring.backend.dto.UserDTO;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/free")
public class AuthRestFreeEndpoint implements IAuthRestFreeEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @SneakyThrows
    @GetMapping(value = "/auth/{username}/{password}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean login(
            @PathVariable @Nullable final String username,
            @PathVariable @Nullable final String password
    ) {
        return userService.login(username, password);
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/profile",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO profile() {
        return UserDTO.toUserDTO(userService.profile());
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/logout",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void logout() {
        userService.logout();
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/login/{login}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO findByLogin(
            @PathVariable @Nullable final String login
    ) {
        return UserDTO.toUserDTO(userService.findByLogin(login));
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/firstName/{firstName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO findUserByFirstName(
            @PathVariable @Nullable final String firstName
    ) {
        return UserDTO.toUserDTO(userService.findUserByFirstName(firstName));
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/secondName/{secondName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO findUserBySecondName(
            @PathVariable @Nullable final String secondName
    ) {
        return UserDTO.toUserDTO(userService.findUserBySecondName(secondName));
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/lastName/{lastName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO findUserByLastName(
            @PathVariable @Nullable final String lastName
    ) {
        return UserDTO.toUserDTO(userService.findUserByLastName(lastName));
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/users/login/{login}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteUserByLogin(
            @PathVariable @Nullable final String login
    ) {
        userService.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PostMapping(value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public User save(
            @RequestBody @Nullable final User user
    ) {
        assert user != null;
        if (user.getPasswordHash() != null)
            user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PutMapping(value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public User update(
            @RequestBody @Nullable final User user
    ) {
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO findById(
            @PathVariable @Nullable final String id
    ) {
        return UserDTO.toUserDTO(userService.findById(id));
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping(value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserDTO> findAll() {
        return UserDTO.toUserDTOList(userService.findAll());
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/users/count",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return userService.count();
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/users/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(
            @PathVariable @Nullable final String id
    ) {
        userService.deleteById(id);
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/users/byBody",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(
            @RequestBody @Nullable final User user
    ) {
        userService.delete(user);
    }

    @Override
    @SneakyThrows
    @DeleteMapping(value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAll() {
        userService.deleteAll();
    }

    @Override
    @SneakyThrows
    @PutMapping(value = "/users/updateFirstName/{id}/{newFirstName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateFirstName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newFirstName
    ) {
        @Nullable User user = userService.findById(id);
        user.setFirstName(newFirstName);
        userService.save(user);
    }

    @Override
    @SneakyThrows
    @PutMapping(value = "/users/updateSecondName/{id}/{newSecondName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateSecondName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newSecondName
    ) {
        @Nullable User user = userService.findById(id);
        user.setSecondName(newSecondName);
        userService.save(user);
    }

    @Override
    @SneakyThrows
    @PutMapping(value = "/users/updateLastName/{id}/{newLastName}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateLastName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newLastName
    ) {
        @Nullable User user = userService.findById(id);
        user.setLastName(newLastName);
        userService.save(user);
    }

}

