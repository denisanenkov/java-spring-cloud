package ru.anenkov.spring.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.spring.backend.dto.ProjectDTO;


public interface ProjectDtoRepository extends JpaRepository<ProjectDTO, String> {
}
