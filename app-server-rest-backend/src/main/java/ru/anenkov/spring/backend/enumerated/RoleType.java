package ru.anenkov.spring.backend.enumerated;

public enum RoleType {
    ADMINISTRATOR,
    USER
}
