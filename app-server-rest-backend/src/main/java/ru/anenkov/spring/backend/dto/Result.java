package ru.anenkov.spring.backend.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    private Boolean success = true;

    public String str = "";

    public Result(@NotNull Boolean success) {
        this.success = success;
    }

    public Result(Exception e) {
        success = false;
    }

}
