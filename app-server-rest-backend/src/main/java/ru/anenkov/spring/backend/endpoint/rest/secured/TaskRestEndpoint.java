package ru.anenkov.spring.backend.endpoint.rest.secured;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.api.endpoint.rest.secured.ITaskRestEndpoint;
import ru.anenkov.spring.backend.dto.TaskDTO;
import ru.anenkov.spring.backend.service.TaskService;
import ru.anenkov.spring.backend.util.UserUtil;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @Nullable
    @GetMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks(
            @PathVariable @Nullable String projectId
    ) {
        return TaskDTO.toTaskListDTO(Objects.requireNonNull
                (taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId)));
    }

    @Override
    @Nullable
    @GetMapping(value = "/{projectId}/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findTaskByUserIdAndProjectIdAndId(
            @PathVariable @Nullable String projectId,
            @PathVariable @Nullable final String id
    ) {
        @Nullable TaskDTO task = TaskDTO.toTaskDTO
                (Objects.requireNonNull(
                        taskService.findTaskByUserIdAndProjectIdAndId
                                (UserUtil.getUserId(), projectId, id)));
        System.out.println("FINDED: " + task);
        return task;
    }

    @Override
    @Nullable
    @PostMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task) {
        taskService.addDTO(task);
        return task;
    }

    @Override
    @Nullable
    @PutMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateTask(
            @RequestBody @Nullable final TaskDTO task
    ) {
        taskService.addDTO(task);
        return task;
    }

    @Override
    @DeleteMapping(value = "/{projectId}/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeTaskByUserIdAndProjectIdAndId(
            @PathVariable @Nullable final String projectId,
            @PathVariable @Nullable final String id
    ) {
        taskService.removeTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id);
    }

    @Override
    @DeleteMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeAllByUserIdAndProjectId(
            @PathVariable @Nullable final String projectId
    ) {
        taskService.removeAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @Override
    @GetMapping(value = "/{projectId}/tasks/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long countByUserIdAndProjectId(
            @PathVariable @Nullable final String projectId
    ) {
        return taskService.countByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

}
