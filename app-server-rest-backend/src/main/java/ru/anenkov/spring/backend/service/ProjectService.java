package ru.anenkov.spring.backend.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.spring.backend.api.service.IProjectService;
import ru.anenkov.spring.backend.dto.ProjectDTO;
import ru.anenkov.spring.backend.exception.empty.EmptyEntityException;
import ru.anenkov.spring.backend.exception.empty.EmptyIdException;
import ru.anenkov.spring.backend.exception.empty.EmptyListException;
import ru.anenkov.spring.backend.model.Project;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.ProjectDtoRepository;
import ru.anenkov.spring.backend.repository.ProjectRepository;
import ru.anenkov.spring.backend.repository.UserRepository;
import ru.anenkov.spring.backend.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Override
    @SneakyThrows
    public Project toProject(@Nullable final Optional<Project> projectDTO) {
        if (projectDTO == null) throw new EmptyEntityException();
        @Nullable Project newProject = new Project();
        newProject.setId(projectDTO.get().getId());
        newProject.setName(projectDTO.get().getName());
        newProject.setDescription(projectDTO.get().getDescription());
        newProject.setDateBegin(projectDTO.get().getDateBegin());
        newProject.setDateFinish(projectDTO.get().getDateFinish());
        newProject.setStatus(projectDTO.get().getStatus());
        return newProject;
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Project> toProjectList(@Nullable final List<Optional<Project>> projectOptDtoList) {
        if (projectOptDtoList == null || projectOptDtoList.isEmpty()) throw new EmptyListException();
        @Nullable List<Project> projectList = new ArrayList<>();
        for (Optional<Project> project : projectOptDtoList) {
            projectList.add(toProject(project));
        }
        return projectList;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        if (project == null) throw new EmptyEntityException();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(projectDTO.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Project project
    ) {
        assert userRepository != null;
        assert projectRepository != null;
        if (project == null) throw new EmptyEntityException();
        @Nullable final User user = userRepository.findById(
                Objects.requireNonNull(UserUtil.getUserId())).orElse(null);
        project.setUser(user);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addDTO(
            @Nullable final ProjectDTO project
    ) {
        if (project == null) throw new EmptyEntityException();
        assert projectDtoRepository != null;
        Project project1 = new Project();
        project1.setId(project.getId());
        project1.setName(project.getName());
        project1.setDescription(project.getDescription());
        projectRepository.save(project1);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        projectRepository.removeAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> findAllByUserId(@Nullable final String userId) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findProjectByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findProjectByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.removeProjectsByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long countByUserId(
            @Nullable final String userId
    ) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return projectRepository.countByUserId(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findProjectByUserIdAndName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        assert projectRepository != null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        return projectRepository.findProjectByUserIdAndName(userId, name);
    }

    /**
     * no secure
     */

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Project findProjectId(@Nullable final String id) {
        assert projectRepository != null;
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> findAllProjects() {
        assert projectRepository != null;
        return projectRepository.findAll();
    }

    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        assert projectRepository != null;
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    public void deleteAll() {
        assert projectRepository != null;
        projectRepository.deleteAll();
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public Project save(@Nullable final Project project) {
        assert projectRepository != null;
        if (project == null) throw new EmptyEntityException();
        return projectRepository.save(project);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        assert projectRepository != null;
        return projectRepository.count();
    }

}
