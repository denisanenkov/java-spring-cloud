package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyUserIdSessionException extends AbstractException {

    public EmptyUserIdSessionException() {
        super("Error! User Id in Session is Empty!");
    }

}
