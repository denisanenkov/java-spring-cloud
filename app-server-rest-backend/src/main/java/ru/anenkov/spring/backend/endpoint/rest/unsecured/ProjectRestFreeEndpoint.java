package ru.anenkov.spring.backend.endpoint.rest.unsecured;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.api.endpoint.rest.secured.IProjectRestEndpoint;
import ru.anenkov.spring.backend.api.endpoint.rest.unsecured.IProjectRestFreeEndpoint;
import ru.anenkov.spring.backend.dto.ProjectDTO;
import ru.anenkov.spring.backend.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/api/free")
public class ProjectRestFreeEndpoint implements IProjectRestFreeEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO add(@RequestBody @Nullable ProjectDTO project) {
        projectService.addDTO(project);
        return project;
    }

    @Override
    @PutMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        projectService.addDTO(project);
        return project;
    }

    @Override
    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectId(id));
    }

    @Override
    @DeleteMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeOneById(@Nullable @PathVariable final String id) {
        projectService.deleteById(id);
    }

    @Override
    @DeleteMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public void removeAllProjects() {
        projectService.deleteAll();
    }

    @Override
    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO(projectService.findAllProjects());
//        return ProjectDTO.toProjectListDTO(projectService.findAllProjects());
    }

    @Override
    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return projectService.count();
    }

}
