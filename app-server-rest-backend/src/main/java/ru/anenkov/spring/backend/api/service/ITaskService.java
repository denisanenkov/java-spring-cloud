package ru.anenkov.spring.backend.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.spring.backend.dto.TaskDTO;
import ru.anenkov.spring.backend.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskService {

    @SneakyThrows
    @Transactional
    void add(@Nullable Task task);

    @SneakyThrows
    @Transactional
    void addDTO(@Nullable TaskDTO task);

    @SneakyThrows
    Task toTask(@Nullable Optional<Task> taskDTO);

    @SneakyThrows
    @Transactional(readOnly = true)
    List<Task> findAllByUserId(@Nullable String userId);

    @SneakyThrows
    List<Task> toTaskList(@Nullable List<Optional<Task>> taskOptDtoList);

    @SneakyThrows
    @Transactional
    Task findTaskByUserIdAndId(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    @Transactional
    void removeTaskByIdAndUserId(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    @Transactional(readOnly = true)
    long countByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    @Transactional
    void removeAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    @Transactional(readOnly = true)
    List<Task> findAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

    @SneakyThrows
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndId(@Nullable String userId, @Nullable String projectId, @Nullable String id);

    @SneakyThrows
    @Transactional
    void removeTaskByUserIdAndProjectIdAndId(@Nullable String userId, @Nullable String projectId, @Nullable String id);

    @SneakyThrows
    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndName(@Nullable String userId, @Nullable String projectId, @Nullable String name);

}
