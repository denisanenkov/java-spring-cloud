package ru.anenkov.spring.backend.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.spring.backend.constant.CredentialsConstant;
import ru.anenkov.spring.backend.dto.CustomUser;
import ru.anenkov.spring.backend.enumerated.RoleType;
import ru.anenkov.spring.backend.exception.empty.EmptyLoginException;
import ru.anenkov.spring.backend.exception.empty.EmptyPasswordException;
import ru.anenkov.spring.backend.model.Role;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UsersDetailsServiceBean implements UserDetailsService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        assert userRepository != null;
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDetails loadUserByUsername
            (@Nullable final String username) throws UsernameNotFoundException {
        if (username == null || username.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @Nullable final List<Role> userRoles = user.getRoles();
        @Nullable final List<String> roles = new ArrayList<>();
        assert userRoles != null;
        for (final Role role : userRoles) roles.add(role.toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    @SneakyThrows
    public void initUsers() {
        assert userRepository != null;
        if (userRepository.count() > 0) return;
        create(CredentialsConstant.FIRST_USER_LOGIN,
                CredentialsConstant.FIRST_USER_PASSWORD,
                RoleType.ADMINISTRATOR);
        create(CredentialsConstant.SECOND_USER_LOGIN,
                CredentialsConstant.SECOND_USER_PASSWORD,
                RoleType.USER);
        create(CredentialsConstant.THIRD_USER_LOGIN,
                CredentialsConstant.THIRD_USER_PASSWORD,
                RoleType.USER);
        create(CredentialsConstant.FOURTH_USER_LOGIN,
                CredentialsConstant.FOURTH_USER_PASSWORD,
                RoleType.USER);
    }

    @Transactional
    @SneakyThrows
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        assert userRepository != null;
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        @Nullable final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

    @SneakyThrows
    private void initializationUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        @Nullable final User user = findByLogin(login);
        if (user != null) return;
        create(login, password, roleType);
    }

}
