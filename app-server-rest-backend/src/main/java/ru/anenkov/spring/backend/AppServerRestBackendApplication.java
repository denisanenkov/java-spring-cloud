package ru.anenkov.spring.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class AppServerRestBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppServerRestBackendApplication.class, args);
    }

}
