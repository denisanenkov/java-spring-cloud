package ru.anenkov.spring.backend.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ru.anenkov.spring.backend.enumerated.RoleType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "app_role")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}
