package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptySignatureSessionException extends AbstractException {

    public EmptySignatureSessionException() {
        super("Error! Session signature is empty!");
    }

}
