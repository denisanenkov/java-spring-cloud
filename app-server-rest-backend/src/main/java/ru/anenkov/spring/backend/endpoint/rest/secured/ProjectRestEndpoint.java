package ru.anenkov.spring.backend.endpoint.rest.secured;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import ru.anenkov.spring.backend.util.UserUtil;
import ru.anenkov.spring.backend.dto.ProjectDTO;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.spring.backend.api.endpoint.rest.secured.IProjectRestEndpoint;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @NotNull
    @PostMapping(value = "/projects")
    public ProjectDTO add(@Nullable @RequestBody ProjectDTO project) {
        assert project != null;
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @Nullable
    @PutMapping(value = "/projects")
    public ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        assert project != null;
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @Nullable
    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return ProjectDTO.toProjectDTO
                (Objects.requireNonNull(projectService.findProjectByIdAndUserId(UserUtil.getUserId(), id)));
    }

    @Override
    @DeleteMapping(value = "/projects/{id}")
    public void removeOneById(@Nullable @PathVariable final String id) {
        @Nullable ProjectDTO project = ProjectDTO.toProjectDTO
                (Objects.requireNonNull(projectService.findProjectByIdAndUserId(UserUtil.getUserId(), id)));
        projectService.removeProjectByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping(value = "/projects")
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO
                (Objects.requireNonNull(projectService.findAllByUserId(UserUtil.getUserId())));
    }

    @Override
    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

}
