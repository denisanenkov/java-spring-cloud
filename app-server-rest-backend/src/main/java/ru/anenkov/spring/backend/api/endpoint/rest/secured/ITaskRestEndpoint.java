package ru.anenkov.spring.backend.api.endpoint.rest.secured;

import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.dto.TaskDTO;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> tasks(
            @PathVariable @Nullable String projectId
    );

    @Nullable
    @GetMapping(value = "/tasks/{projectId}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO findTaskByUserIdAndProjectIdAndId(
            @PathVariable @Nullable String projectId,
            @PathVariable @Nullable String id
    );

    @Nullable
    @PostMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO addTask(@RequestBody @Nullable TaskDTO task);

    @Nullable
    @PutMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO updateTask(@RequestBody @Nullable TaskDTO task);

    @DeleteMapping(value = "/tasks/{projectId}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void removeTaskByUserIdAndProjectIdAndId(
            @PathVariable @Nullable String projectId,
            @PathVariable @Nullable String id
    );

    @DeleteMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    void removeAllByUserIdAndProjectId(
            @PathVariable @Nullable String projectId
    );

    @GetMapping(value = "/{projectId}/tasks/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long countByUserIdAndProjectId(
            @PathVariable @Nullable String projectId
    );

}
