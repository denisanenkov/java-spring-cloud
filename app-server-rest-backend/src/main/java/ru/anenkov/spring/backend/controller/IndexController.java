package ru.anenkov.spring.backend.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.anenkov.spring.backend.dto.CustomUser;
import ru.anenkov.spring.backend.model.Task;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.UserRepository;
import ru.anenkov.spring.backend.util.UserUtil;

@Controller
public class IndexController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/profile")
    public String index(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @Nullable Model model
    ) {
        User user1 = userRepository.findById(UserUtil.getUserId()).orElse(null);
        model.addAttribute("user", user1);
        return "profile";
    }

    @GetMapping("/profile/edit")
    public String editUser(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @Nullable Model model
    ) {
        User user1 = userRepository.findById(UserUtil.getUserId()).orElse(null);
        model.addAttribute("user", user1);
        return "profiledit";
    }

    @PostMapping("/profile/edit/save")
    public String saveUser(
            @AuthenticationPrincipal @Nullable final CustomUser user,
            @ModelAttribute("user") User user1
    ) {
        user1.setId(user.getUserId());
        User optional = userRepository.findById(user.getUserId()).orElse(null);
        user1.setPasswordHash(optional.getPasswordHash());
        user1.setLogin(optional.getLogin());
        userRepository.save(user1);
        return "redirect:/profile";
    }

}

