package ru.anenkov.spring.backend.api.endpoint.rest.unsecured;

import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.dto.TaskDTO;

import java.util.List;

public interface ITaskRestFreeEndpoint {

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> tasks();

    @GetMapping(value = "/port", produces = MediaType.APPLICATION_JSON_VALUE)
    String getPort(
    );

    @GetMapping(value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO getTask(
            @PathVariable @Nullable String id
    );

    @PostMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO addTask(@RequestBody @Nullable TaskDTO task);

    @PutMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO updateTask(@RequestBody @Nullable TaskDTO task);

    @DeleteMapping(value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    String deleteTask(
            @PathVariable @Nullable String id
    );

    @DeleteMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteAllTasks();

    @GetMapping(value = "/tasks/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long count();

}
