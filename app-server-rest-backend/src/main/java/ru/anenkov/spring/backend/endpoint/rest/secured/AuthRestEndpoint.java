package ru.anenkov.spring.backend.endpoint.rest.secured;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.dto.UserDTO;
import ru.anenkov.spring.backend.util.UserUtil;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.anenkov.spring.backend.api.endpoint.rest.secured.IAuthRestEndpoint;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @SneakyThrows
    @GetMapping(value = "/auth/{username}/{password}")
    public boolean login(
            @PathVariable @Nullable final String username,
            @PathVariable @Nullable final String password
    ) {
        return userService.login(username, password);
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/profile")
    public User profile() {
        return userService.profile();
    }

    @Override
    @SneakyThrows
    @GetMapping(value = "/logout")
    public void logout() {
        userService.logout();
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/login/{login}")
    public UserDTO findByLogin(
            @PathVariable @Nullable final String login
    ) {
        return UserDTO.toUserDTO(userService.findByLogin(login));
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/firstName/{firstName}")
    public UserDTO findUserByFirstName(
            @PathVariable @Nullable final String firstName
    ) {
        return UserDTO.toUserDTO(userService.findUserByFirstName(firstName));
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/secondName/{secondName}")
    public UserDTO findUserBySecondName(
            @PathVariable @Nullable final String secondName
    ) {
        return UserDTO.toUserDTO(userService.findUserBySecondName(secondName));
    }

    @Override
    @Nullable
    @SneakyThrows
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @GetMapping(value = "/users/lastName/{lastName}")
    public UserDTO findUserByLastName(
            @PathVariable @Nullable final String lastName
    ) {
        return UserDTO.toUserDTO(userService.findUserByLastName(lastName));
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/login/{login}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteUserByLogin(
            @PathVariable @Nullable final String login
    ) {
        userService.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PostMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User save(
            @RequestBody @Nullable final User user
    ) {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @PutMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User update(
            @RequestBody @Nullable final User user
    ) {
        return userService.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDTO findById(
            @PathVariable @Nullable final String id
    ) {
        return UserDTO.toUserDTO(userService.findById(id));
    }

    @Override
    @SneakyThrows
    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<UserDTO> findAll() {
        return UserDTO.toUserDTOList(userService.findAll());
    }

    @Override
    @SneakyThrows
    @GetMapping("/users/count")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public long count() {
        return userService.count();
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteById(
            @PathVariable @Nullable final String id
    ) {
        userService.deleteById(id);
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users/byBody")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(
            @RequestBody @Nullable final User user
    ) {
        userService.delete(user);
    }

    @Override
    @SneakyThrows
    @DeleteMapping("/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteAll() {
        userService.deleteAll();
    }

    @Override
    @SneakyThrows
    @PutMapping("/users/updateFirstName/{newFirstName}")
    public void updateFirstName(
            @PathVariable @Nullable final String newFirstName
    ) {
        @Nullable User user = userService.findById(UserUtil.getUserId());
        user.setFirstName(newFirstName);
        userService.save(user);
    }

    @SneakyThrows
    @PutMapping("/users/updateSecondName/{newSecondName}")
    public void updateSecondName(
            @PathVariable @Nullable final String newSecondName
    ) {
        @Nullable User user = userService.findById(UserUtil.getUserId());
        user.setSecondName(newSecondName);
        userService.save(user);
    }

    @SneakyThrows
    @PutMapping("/users/updateLastName/{newLastName}")
    public void updateLastName(
            @PathVariable @Nullable final String newLastName
    ) {
        @Nullable User user = userService.findById(UserUtil.getUserId());
        user.setLastName(newLastName);
        userService.save(user);
    }

}
