package ru.anenkov.spring.backend.api.endpoint.rest.unsecured;

import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.api.endpoint.rest.secured.IProjectRestEndpoint;
import ru.anenkov.spring.backend.dto.ProjectDTO;

import java.util.List;

public interface IProjectRestFreeEndpoint extends IProjectRestEndpoint {
    @Override
    @PostMapping(value = "/projects")
    ProjectDTO add(@RequestBody @Nullable ProjectDTO project);

    @Override
    @PutMapping(value = "/projects")
    ProjectDTO update(@Nullable @RequestBody ProjectDTO project);

    @Override
    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") String id);

    @Override
    @DeleteMapping(value = "/projects/{id}")
    void removeOneById(@Nullable @PathVariable String id);

    @Override
    @DeleteMapping(value = "/projects")
    void removeAllProjects();

    @Override
    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDTO> getListByUserId();

    @Override
    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long count();
}
