package ru.anenkov.spring.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.spring.backend.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "app_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends AbstractEntity {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "name")
    private String name = "";

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @ManyToOne
    @JsonBackReference
    private User user;

    @JsonManagedReference
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "[PROJECT" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", NAME = '" + name + "';\n" +
                ", DESCRIPTION = '" + description + "';\n" +
                ", STATUS = " + status + "';\n" +
                ", DATE BEGIN = " + dateBegin + "';\n" +
                ", DATE FINISH = " + dateFinish + "';]\n\n" +
                ", USER ID = " + user.getId() + "';\n";
    }

}
