package ru.anenkov.spring.backend.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.spring.backend.api.service.IUserService;
import ru.anenkov.spring.backend.dto.UserDTO;
import ru.anenkov.spring.backend.exception.empty.EmptyEntityException;
import ru.anenkov.spring.backend.exception.empty.EmptyIdException;
import ru.anenkov.spring.backend.exception.empty.EmptyLoginException;
import ru.anenkov.spring.backend.exception.empty.EmptyParameterException;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.UserRepository;

import javax.annotation.Resource;
import javax.jws.WebParam;
import java.util.List;
import java.util.Objects;

@Service
public class UserService implements IUserService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Resource
    private AuthenticationManager authenticationManager;

    @Nullable
    @SneakyThrows
    public User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new EmptyEntityException();
        User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setFirstName(userDTO.getFirstName());
        user.setSecondName(userDTO.getSecondName());
        user.setLastName(userDTO.getLastName());
        return user;
    }

    @Override
    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            @Nullable final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            assert authenticationManager != null;
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        assert userRepository != null;
        return userRepository.findByLogin(username);
    }

    @Override
    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        assert userRepository != null;
        return userRepository.findByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByFirstName(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return Objects.requireNonNull(findAllByFirstName(firstName)).get(0);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserBySecondName(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return Objects.requireNonNull(findAllBySecondName(secondName)).get(0);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByLastName(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return Objects.requireNonNull(findAllByLastName(lastName)).get(0);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void deleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        assert userRepository != null;
        userRepository.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public User save(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        assert userRepository != null;
        return userRepository.save(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        assert userRepository != null;
        return userRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> findAll() {
        assert userRepository != null;
        return userRepository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        assert userRepository != null;
        return userRepository.count();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        assert userRepository != null;
        userRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void delete(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        assert userRepository != null;
        userRepository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public void deleteAll() {
        assert userRepository != null;
        userRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        assert userRepository != null;
        return userRepository.existsById(id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<User> findAllByFirstName(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyParameterException();
        assert userRepository != null;
        return userRepository.findAllByFirstName(firstName);
    }

    @Nullable
    @Transactional(readOnly = true)
    public List<User> findAllBySecondName(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyParameterException();
        assert userRepository != null;
        return userRepository.findAllBySecondName(secondName);
    }

    @Nullable
    @Transactional(readOnly = true)
    public List<User> findAllByLastName(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyParameterException();
        assert userRepository != null;
        return userRepository.findAllByLastName(lastName);
    }

}
