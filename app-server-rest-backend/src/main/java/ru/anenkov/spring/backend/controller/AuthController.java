package ru.anenkov.spring.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.UserRepository;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @GetMapping(value = "/login", produces = "application/json")
    public boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return (authentication.isAuthenticated());
        } catch (final Exception ex) {
            return false;
        }
    }

    @GetMapping(value = "/session", produces = "application/json")
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping(value = "/user", produces = "application/json")
    public User user(@AuthenticationPrincipal(errorOnInvalidType = true) final User user) {
        return user;
    }

    @GetMapping(value = "/profile", produces = "application/json")
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

}
