package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User Id is empty!");
    }

}
