package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyTimestampSessionException extends AbstractException {

    public EmptyTimestampSessionException() {
        super("Error! Session timestamp is empty!");
    }

}
