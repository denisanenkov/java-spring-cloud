package ru.anenkov.spring.backend.endpoint.rest.unsecured;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.backend.api.endpoint.rest.unsecured.ITaskRestFreeEndpoint;
import ru.anenkov.spring.backend.dto.TaskDTO;
import ru.anenkov.spring.backend.exception.rest.NoSuchEntitiesException;
import ru.anenkov.spring.backend.service.TaskService;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/free")
public class TaskRestFreeEndpoint implements ITaskRestFreeEndpoint {

    @Autowired
    private TaskService taskService;

    @Value("${server.port}")
    private String port;

    @Override
    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks() {
        return TaskDTO.toTaskListDTO(Objects.requireNonNull(taskService.findAllTasks()));
    }

    @Override
    @GetMapping(value = "/port", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getPort(
    ) {
        return port;
    }

    @Override
    @GetMapping(value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO getTask(
            @PathVariable @Nullable final String id
    ) {
        TaskDTO task = TaskDTO.toTaskDTO
                (Objects.requireNonNull(taskService.findProjectId(id)));
        System.out.println("FINDED: " + task);
        return task;
    }

    @Override
    @PostMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task) {
        taskService.addDTO(task);
        return task;
    }

    @Override
    @PutMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateTask(@RequestBody @Nullable TaskDTO task) {
        taskService.addDTO(task);
        return task;
    }

    @Override
    @DeleteMapping(value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteTask(
            @PathVariable @Nullable final String id
    ) {
        taskService.deleteById(id);
        return "Task with id " + id + " was deleted successfully!";
    }

    @Override
    @DeleteMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAllTasks() {
        taskService.deleteAll();
    }

    @Override
    @GetMapping(value = "/tasks/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return taskService.count();
    }

}
