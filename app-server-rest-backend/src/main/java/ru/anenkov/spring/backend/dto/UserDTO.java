package ru.anenkov.spring.backend.dto;

import ru.anenkov.spring.backend.model.Role;
import ru.anenkov.spring.backend.model.User;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id = UUID.randomUUID().toString();

    private String login = "";

    private String passwordHash = "";

    private String firstName = "";

    private String secondName = "";

    private String lastName = "";

    private List<String> roles = new ArrayList<>();

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String lastName
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
    }

    @NotNull
    public static UserDTO toUserDTO(@Nullable final User user) {
        @NotNull UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setSecondName(user.getSecondName());
        userDTO.setLastName(user.getLastName());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setLogin(user.getLogin());
        userDTO.setRoles(toStringRoles(user.getRoles()));
        return userDTO;
    }

    @NotNull
    public static List<UserDTO> toUserDTOList(@Nullable final List<User> users) {
        @NotNull List<UserDTO> userDTOList = new ArrayList<>();
        for (@Nullable User user : users) {
            userDTOList.add(toUserDTO(user));
        }
        return userDTOList;
    }

    @NotNull
    public static List<String> toStringRoles(@NotNull final List<Role> roles) {
        @NotNull List<String> userRoles = new ArrayList<>();
        for (@Nullable Role role : roles) {
            userRoles.add(role.toString());
        }
        return userRoles;
    }

}