package ru.anenkov.spring.backend.endpoint.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.api.endpoint.soap.IAuthEndpoint;
import ru.anenkov.spring.backend.exception.empty.EmptyEntityException;
import ru.anenkov.spring.backend.exception.empty.EmptyIdException;
import ru.anenkov.spring.backend.exception.empty.EmptyLoginException;
import ru.anenkov.spring.backend.model.User;
import ru.anenkov.spring.backend.repository.UserRepository;
import ru.anenkov.spring.backend.service.UserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * address: http://localhost:8282/services/AuthEndpoint?wsdl
 * 2th address: http://localhost:8383/services/AuthEndpoint?wsdl
 */

@Component
@WebService
public class AuthEndpoint implements IAuthEndpoint {

    @Nullable
    @Autowired
    private UserService userService;

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @Override
    @WebMethod
    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @Nullable final String username = authentication.getName();
        assert userService != null;
        return userService.findByLogin(username);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        assert userService != null;
        return userService.findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByFirstName(
            @Nullable final @WebParam(name = "firstName") String firstName
    ) {
        assert userService != null;
        return userService.findUserByFirstName(firstName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserBySecondName(
            @Nullable final @WebParam(name = "secondName") String secondName
    ) {
        assert userService != null;
        return userService.findUserBySecondName(secondName);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByLastName(
            @Nullable final @WebParam(name = "lastName") String lastName
    ) {
        assert userService != null;
        return userService.findUserByLastName(lastName);
    }

    @Override
    @SneakyThrows
    public void deleteUserByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        assert userService != null;
        userService.deleteUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User saveUser(
            @Nullable final @WebParam(name = "user") User user
    ) {
        assert userService != null;
        return userService.save(user);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        assert userService != null;
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public long countUsers() {
        return userService.count();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        assert userService != null;
        userService.deleteById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteUser(
            @Nullable final @WebParam(name = "user") User user
    ) {
        assert userService != null;
        userService.delete(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteAllUsers() {
        assert userService != null;
        userService.deleteAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsUserById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        assert userService != null;
        return userService.existsById(id);
    }

}
