package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptyManagerException extends AbstractException {

    public EmptyManagerException() {
        super("Error! Problems with connecting to the database..");
    }

}
