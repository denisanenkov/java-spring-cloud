package ru.anenkov.spring.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.spring.backend.enumerated.Status;
import ru.anenkov.spring.backend.model.Project;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_project")
public class ProjectDTO extends AbstractEntityDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    private String description;

    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "user_id")
    private String userId;

    public ProjectDTO(String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final String userId
    ) {
        this.name = name;
        this.description = description;
        this.userId = userId;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateFinish
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "[PROJECT" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", NAME = '" + name + "';\n" +
                ", DESCRIPTION = '" + description + "';\n" +
                ", STATUS = " + status + "';\n" +
                ", DATE BEGIN = " + dateBegin + "';\n" +
                ", DATE FINISH = " + dateFinish + "';]\n\n" +
                ", USER ID = " + userId + "';\n";
    }

    @NotNull
    public static ProjectDTO toProjectDTO(@NotNull final Project project) {
        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setDateBegin(project.getDateBegin());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setDescription(project.getDescription());
        if (project.getUser() != null) projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    @NotNull
    public static List<ProjectDTO> toProjectListDTO(@NotNull final List<Project> projectList) {
        @NotNull List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (@Nullable Project project : projectList) {
            projectDTOList.add(toProjectDTO(project));
        }
        return projectDTOList;
    }

}
