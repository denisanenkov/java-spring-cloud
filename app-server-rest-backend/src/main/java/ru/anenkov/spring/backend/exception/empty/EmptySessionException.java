package ru.anenkov.spring.backend.exception.empty;

import ru.anenkov.spring.backend.exception.AbstractException;

public class EmptySessionException extends AbstractException {

    public EmptySessionException() {
        System.out.println("Error! Session is null..");
    }

}
