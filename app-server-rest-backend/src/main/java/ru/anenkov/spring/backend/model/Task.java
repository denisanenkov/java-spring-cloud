package ru.anenkov.spring.backend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.spring.backend.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "name")
    private String name = "";

    @Column(name = "description")
    private String description = "";

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @ManyToOne
    @JsonBackReference
    private Project project;

    @ManyToOne
    @JsonBackReference
    private User user;

    public Task(String name) {
        this.name = name;
    }

    public Task(Project project) {
        this.project = project;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "[TASK" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", NAME = '" + name + "';\n" +
                ", DESCRIPTION = '" + description + "';\n" +
                ", STATUS = " + status + "';\n" +
                ", DATE BEGIN = " + dateBegin + "';\n" +
                ", DATE FINISH = " + dateFinish + "';]\n" +
                ", PROJECT ID = " + project.getId() + "';\n" +
                ", USER ID = " + user.getId() + "';\n";
    }

}
