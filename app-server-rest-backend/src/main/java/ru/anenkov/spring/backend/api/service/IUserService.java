package ru.anenkov.spring.backend.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.spring.backend.model.User;

import javax.jws.WebParam;
import java.util.List;

public interface IUserService {

    @Nullable
    @SneakyThrows
    User profile();

    @SneakyThrows
    boolean logout();

    @Nullable
    @SneakyThrows
    @Transactional
    User save(@Nullable User user);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    List<User> findAll();

    @SneakyThrows
    @Transactional(readOnly = true)
    long count();

    @SneakyThrows
    @Transactional(readOnly = true)
    void deleteAll();

    @SneakyThrows
    @Transactional
    void delete(@Nullable User user);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    User findById(@Nullable String id);

    @SneakyThrows
    @Transactional
    void deleteById(@Nullable String id);

    @SneakyThrows
    @Transactional(readOnly = true)
    boolean existsById(@Nullable String id);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    User findByLogin(@Nullable String login);

    @Transactional
    @SneakyThrows
    void deleteUserByLogin(@Nullable String login);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    User findUserByLastName(@Nullable String lastName);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    User findUserByFirstName(@Nullable String firstName);

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    User findUserBySecondName(@Nullable String secondName);

    @Transactional(readOnly = true)
    @Nullable List<User> findAllByFirstName(@Nullable String firstName);

    @SneakyThrows
    boolean login(@Nullable @WebParam(name = "username") String username, @Nullable @WebParam(name = "password") String password);

}
