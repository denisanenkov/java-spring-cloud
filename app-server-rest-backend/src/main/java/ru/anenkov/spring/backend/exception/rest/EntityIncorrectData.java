package ru.anenkov.spring.backend.exception.rest;

public class EntityIncorrectData {

    private String information = "";

    public EntityIncorrectData() {
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

}
