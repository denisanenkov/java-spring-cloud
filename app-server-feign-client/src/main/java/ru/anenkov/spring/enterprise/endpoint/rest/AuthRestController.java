package ru.anenkov.spring.enterprise.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.enterprise.client.rest.IAuthRestController;
import ru.anenkov.spring.enterprise.model.User;

@RestController
@RequestMapping("/feign/api/free")
public class AuthRestController {

    @Autowired
    IAuthRestController iAuthRestController;

    @GetMapping(value = "/auth/{username}/{password}")
    boolean login(
            @PathVariable @Nullable final String username,
            @PathVariable @Nullable final String password
    ) {
        return iAuthRestController.login(username, password);
    }

    ;

    @GetMapping(value = "/profile")
    User profile() {
        return iAuthRestController.profile();
    }

    ;

    @GetMapping(value = "/logout")
    void logout() {
        iAuthRestController.logout();
    }

    ;

    @Nullable
    @GetMapping(value = "/users/login/{login}")
    User findByLogin(@PathVariable @Nullable final String login) {
        return iAuthRestController.findByLogin(login);
    }

    ;

    @Nullable
    @GetMapping(value = "/users/firstName/{firstName}")
    User findUserByFirstName(
            @PathVariable @Nullable final String firstName
    ) {
        return iAuthRestController.findUserByFirstName(firstName);
    }

    ;

    @Nullable
    @GetMapping(value = "/users/secondName/{secondName}")
    User findUserBySecondName(
            @PathVariable @Nullable final String secondName
    ) {
        return iAuthRestController.findUserBySecondName(secondName);
    }

    ;

    @Nullable
    @GetMapping(value = "/users/lastName/{lastName}")
    User findUserByLastName(
            @PathVariable @Nullable final String lastName
    ) {
        return iAuthRestController.findUserByLastName(lastName);
    }

    ;

    @DeleteMapping("/users/login/{login}")
    void deleteUserByLogin(
            @PathVariable @Nullable final String login
    ) {
        iAuthRestController.deleteUserByLogin(login);
    }

    ;

    @Nullable
    @PostMapping("/users")
    User save(
            @RequestBody @Nullable final User user
    ) {
        return iAuthRestController.save(user);
    }

    ;

    @Nullable
    @PutMapping("/users")
    User update(
            @RequestBody @Nullable final User user
    ) {
        return iAuthRestController.update(user);
    }

    ;

    @Nullable
    @GetMapping("/users/{id}")
    User findById(
            @PathVariable @Nullable final String id
    ) {
        return iAuthRestController.findById(id);
    }

    ;

    @Nullable
    @GetMapping("/users")
    Iterable<User> findAll() {
        return iAuthRestController.findAll();
    }

    ;

    @GetMapping("/users/count")
    long count() {
        return iAuthRestController.count();
    }

    ;

    @DeleteMapping("/users/{id}")
    void deleteById(
            @PathVariable @Nullable final String id
    ) {
        iAuthRestController.deleteById(id);
    }

    ;

    @DeleteMapping("/users/byBody")
    void delete(
            @RequestBody @Nullable final User user
    ) {
        iAuthRestController.delete(user);
    }

    ;

    @DeleteMapping("/users")
    void deleteAll() {
        iAuthRestController.deleteAll();
    }

    ;

    @PutMapping("/users/updateFirstName/{id}/{newFirstName}")
    void updateFirstName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newFirstName
    ) {
        iAuthRestController.updateFirstName(id, newFirstName);
    }

    ;

    @PutMapping("/users/updateSecondName/{id}/{newSecondName}")
    void updateSecondName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newSecondName
    ) {
        iAuthRestController.updateSecondName(id, newSecondName);
    }

    ;

    @PutMapping("/users/updateLastName/{id}/{newLastName}")
    void updateLastName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newLastName
    ) {
        iAuthRestController.updateLastName(id, newLastName);
    }

    ;

}
