package ru.anenkov.spring.enterprise.client.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.enterprise.model.User;

@FeignClient("app-server-api")
@RequestMapping("/api/free")
public
interface IAuthRestController {

    @GetMapping(value = "/auth/{username}/{password}")
    boolean login(
            @PathVariable @Nullable final String username,
            @PathVariable @Nullable final String password
    );

    @GetMapping(value = "/profile")
    User profile();

    @GetMapping(value = "/logout")
    void logout();

    @Nullable
    @GetMapping(value = "/users/login/{login}")
    User findByLogin(
            @PathVariable @Nullable final String login
    );

    @Nullable
    @GetMapping(value = "/users/firstName/{firstName}")
    User findUserByFirstName(
            @PathVariable @Nullable final String firstName
    );

    @Nullable
    @GetMapping(value = "/users/secondName/{secondName}")
    User findUserBySecondName(
            @PathVariable @Nullable final String secondName
    );

    @Nullable
    @GetMapping(value = "/users/lastName/{lastName}")
    User findUserByLastName(
            @PathVariable @Nullable final String lastName
    );

    @DeleteMapping("/users/login/{login}")
    void deleteUserByLogin(
            @PathVariable @Nullable final String login
    );

    @Nullable
    @PostMapping("/users")
    User save(
            @RequestBody @Nullable final User user
    );

    @Nullable
    @PutMapping("/users")
    User update(
            @RequestBody @Nullable final User user
    );

    @Nullable
    @GetMapping("/users/{id}")
    User findById(
            @PathVariable @Nullable final String id
    );

    @Nullable
    @GetMapping("/users")
    Iterable<User> findAll();

    @GetMapping("/users/count")
    long count();

    @DeleteMapping("/users/{id}")
    void deleteById(
            @PathVariable @Nullable final String id
    );

    @DeleteMapping("/users/byBody")
    void delete(
            @RequestBody @Nullable final User user
    );

    @DeleteMapping("/users")
    void deleteAll();

    @PutMapping("/users/updateFirstName/{id}/{newFirstName}")
    void updateFirstName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newFirstName
    );

    @PutMapping("/users/updateSecondName/{id}/{newSecondName}")
    void updateSecondName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newSecondName
    );

    @PutMapping("/users/updateLastName/{id}/{newLastName}")
    void updateLastName(
            @PathVariable @Nullable final String id,
            @PathVariable @Nullable final String newLastName
    );

}
