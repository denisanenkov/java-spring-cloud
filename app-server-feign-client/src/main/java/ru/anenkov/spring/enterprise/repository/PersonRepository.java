package ru.anenkov.spring.enterprise.repository;

import org.springframework.stereotype.Repository;
import ru.anenkov.spring.enterprise.model.Person;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository {

    List<Person> personList = new ArrayList<>();

    {
        save(new Person("Denis", "Anenkov", 2000));
        save(new Person("Ivan", "Rakhmetov", 2550));
        save(new Person("Nikita", "Rybakov", 3700));
        save(new Person("Maksim", "Grishin", 1800));
    }

    public Person save(Person person) {
        personList.add(person);
        return person;
    }

    public List<Person> getPersonList() {
        return personList;
    }

}
