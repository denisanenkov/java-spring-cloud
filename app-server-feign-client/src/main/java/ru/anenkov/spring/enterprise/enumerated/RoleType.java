package ru.anenkov.spring.enterprise.enumerated;

public enum RoleType {
    ADMINISTRATOR,
    USER
}
