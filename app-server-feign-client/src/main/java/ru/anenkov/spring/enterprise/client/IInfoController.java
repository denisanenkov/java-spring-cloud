package ru.anenkov.spring.enterprise.client;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.cloud.openfeign.FeignClient;
import ru.anenkov.spring.enterprise.model.Person;

import java.util.List;

@FeignClient("app-server-api")
@RequestMapping("/api/info")
public interface IInfoController {

    @GetMapping(value = "/email")
    String email();

    @GetMapping(value = "/port")
    String currentPort();

}

