package ru.anenkov.spring.enterprise.model;

public class Person {

    String name;

    String secondName;

    Integer salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Person(String name, String secondName, Integer salary) {
        this.name = name;
        this.secondName = secondName;
        this.salary = salary;
    }

}
