package ru.anenkov.spring.enterprise.client.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.enterprise.dto.ProjectDTO;

import java.util.List;

@FeignClient("app-server-api")
@RequestMapping("/api/free")
public interface IProjectRestController {

    @PostMapping(value = "/projects")
    ProjectDTO add(@RequestBody @Nullable ProjectDTO project);

    @PutMapping(value = "/projects")
    ProjectDTO update(@Nullable @RequestBody ProjectDTO project);

    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Nullable
    ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id);

    @DeleteMapping(value = "/projects/{id}")
    String removeOneById(@Nullable @PathVariable final String id);

    @DeleteMapping(value = "/projects")
    void removeAllProjects();

    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDTO> getListByUserId();

    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long count();

}
