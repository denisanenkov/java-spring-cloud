package ru.anenkov.spring.enterprise.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.anenkov.spring.enterprise.client.IInfoController;
import ru.anenkov.spring.enterprise.model.Person;
import ru.anenkov.spring.enterprise.repository.PersonRepository;

import java.util.List;

@RestController
@RequestMapping("/feign/api/free")
public class InfoController {

    @Autowired
    IInfoController infoController;

    @GetMapping(value = "/email")
    public String email() {
        return infoController.email();
    }

    @GetMapping(value = "/port")
    public String currentPort() {
        return infoController.currentPort();
    }

}