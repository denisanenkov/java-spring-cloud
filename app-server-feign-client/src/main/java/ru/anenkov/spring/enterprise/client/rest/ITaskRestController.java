package ru.anenkov.spring.enterprise.client.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.enterprise.dto.TaskDTO;

import java.util.List;

@FeignClient("app-server-api")
@RequestMapping("/api/free")
public interface ITaskRestController {

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks();

    @GetMapping(value = "/tasks/{id}")
    public TaskDTO getTask(
            @PathVariable @Nullable final String id
    );

    @PostMapping(value = "/tasks")
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task);

    @PutMapping(value = "/tasks")
    public TaskDTO updateTask(@RequestBody @Nullable TaskDTO task);

    @DeleteMapping(value = "/tasks/{id}")
    public String deleteTask(
            @PathVariable @Nullable final String id
    );

    @DeleteMapping(value = "/tasks")
    public void deleteAllTasks();

    @GetMapping(value = "/tasks/count")
    public long count();

}
