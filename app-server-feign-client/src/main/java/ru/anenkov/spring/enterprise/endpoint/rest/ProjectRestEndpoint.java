package ru.anenkov.spring.enterprise.endpoint.rest;

import ru.anenkov.spring.enterprise.client.rest.IProjectRestController;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.spring.enterprise.dto.ProjectDTO;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;

import java.util.List;

@RestController
@RequestMapping("/feign/api/free")
public class ProjectRestEndpoint {

    @Autowired
    IProjectRestController projectRestController;

    @PostMapping(value = "/projects")
    ProjectDTO add(@RequestBody @Nullable ProjectDTO project) {
        return projectRestController.add(project);
    }

    ;

    @PutMapping(value = "/projects")
    ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        return projectRestController.update(project);
    }

    ;

    @GetMapping(value = "/projects/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Nullable
    ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return projectRestController.findOneByIdEntity(id);
    }

    ;

    @DeleteMapping(value = "/projects/{id}")
    String removeOneById(@Nullable @PathVariable final String id) {
        return projectRestController.removeOneById(id);
    }

    ;

    @DeleteMapping(value = "/projects")
    void removeAllProjects() {
        projectRestController.removeAllProjects();
    }

    ;

    @Nullable
    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDTO> getListByUserId() {
        return projectRestController.getListByUserId();
    }

    ;

    @GetMapping(value = "/projects/count", produces = MediaType.APPLICATION_JSON_VALUE)
    long count() {
        return projectRestController.count();
    }

    ;

}
