package ru.anenkov.spring.enterprise.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.spring.enterprise.client.rest.ITaskRestController;
import ru.anenkov.spring.enterprise.dto.TaskDTO;

import java.util.List;

@RestController
@RequestMapping("/feign/api/free")
public class TaskRestController {

    @Autowired
    ITaskRestController taskRestController;

    @GetMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks() {
        return taskRestController.tasks();
    }

    ;

    @GetMapping(value = "/tasks/{id}")
    public TaskDTO getTask(
            @PathVariable @Nullable final String id
    ) {
        return taskRestController.getTask(id);
    }

    ;

    @PostMapping(value = "/tasks")
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task) {
        return taskRestController.addTask(task);
    }

    ;

    @PutMapping(value = "/tasks")
    public TaskDTO updateTask(@RequestBody @Nullable TaskDTO task) {
        return taskRestController.updateTask(task);
    }

    ;

    @DeleteMapping(value = "/tasks/{id}")
    public String deleteTask(
            @PathVariable @Nullable final String id
    ) {
        return taskRestController.deleteTask(id);
    }

    ;

    @DeleteMapping(value = "/tasks")
    public void deleteAllTasks() {
        taskRestController.deleteAllTasks();
    }

    ;

    @GetMapping(value = "/tasks/count")
    public long count() {
        return taskRestController.count();
    }

    ;

}
