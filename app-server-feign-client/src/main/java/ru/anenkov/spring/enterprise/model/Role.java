package ru.anenkov.spring.enterprise.model;


import ru.anenkov.spring.enterprise.enumerated.RoleType;

import java.util.UUID;

public class Role {

    private String id = UUID.randomUUID().toString();

    private User user;

    private RoleType roleType = RoleType.USER;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return roleType.name();
    }

}
