# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

- NAME: Anenkov Denis

- E-mail: denk.an@inbox.ru

- Second E-mail: denk.an@bk.ru

# SOFTWARE

JDK version: 1.8

OS: MS Windows 10

# TECHNOLOGY STACK

- JAVA

- MAVEN

- HIBERNATE

- SPRING:
    1. DATA (To provide a familiar and consistent, Spring-based programming model for data access while still retaining
       the special traits of the underlying data store.)
    2. REST (To building web services on the web because they’re easy to build and easy to consume.)
    3. SOAP (To facilitates creating document-driven, contract-first SOAP web services.)
    4. BOOT (To makes it easy to create stand-alone, production-grade Spring based Applications.)
    5. TEST (To create test.)
    6. SECURITY (To provide both authentication and authorization to Java application.)

# PROJECT MODULES

1. app-server-config
2. app-eureka-server
3. app-server-rest-backend
4. app-server-feign-client
5. app-server-zuul-proxy
6. app-soap-client
7. app-rest-client

# MAVEN PROJECT BUILD

```bash
cd project_directory

mvn clean

mvn install
``` 
