package ru.anenkov.tm.communication;

import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.UserDTO;
import ru.anenkov.tm.entiity.User;

import java.util.List;

public class AuthCommunication {

    private final String URL = "http://localhost:8282/api/free";

    @Nullable
    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public static void main(String[] args) {
        AuthCommunication authCommunication = new AuthCommunication();
        authCommunication.addUser(new User("123", "123",
                "Ivan", "Denisovich", "Aristarckov"));
    }

    public List<UserDTO> users() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<List<UserDTO>> responseEntity =
                restTemplate.exchange(URL + "/users",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<UserDTO>>() {
                        });
        @Nullable final List<UserDTO> userList = responseEntity.getBody();
        return userList;

    }

    public UserDTO getUserById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        @Nullable ResponseEntity<UserDTO> responseEntity =
                restTemplate.exchange(URL + "/users/" + id,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<UserDTO>() {
                        });
        @Nullable final UserDTO userDTO = responseEntity.getBody();
        return userDTO;
    }

    public void addUser(@Nullable final User user) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        if (!isExists(user.getId())) {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<User> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<User>() {
                    });
        } else {
            @Nullable HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            @Nullable HttpEntity<User> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<User>() {
                    });
        }
    }

    public void deleteUserById(@Nullable final String id) {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/users/" + id);
        System.out.println("User with id \"" + id + "\" was deleted");
    }

    public void deleteAllUsers() {
        @Nullable final RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(URL + "/users");
    }

    public boolean isExists(@Nullable final String id) {
        @Nullable final List<UserDTO> userList = users();
        for (UserDTO currentUser : userList) {
            if (id.equals(currentUser.getId())) return true;
        }
        return false;
    }

}
