package ru.anenkov.tm.dto;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.entiity.Role;
import ru.anenkov.tm.entiity.User;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id = UUID.randomUUID().toString();

    private String login = "";

    private String passwordHash = "";

    private String firstName = "";

    private String secondName = "";

    private String lastName = "";

    private List<String> roles = new ArrayList<>();

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String lastName
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
    }

    @NotNull
    public static UserDTO toUserDTO(@Nullable final User user) {
        @NotNull UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setSecondName(user.getSecondName());
        userDTO.setLastName(user.getLastName());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setLogin(user.getLogin());
        userDTO.setRoles(toStringRoles(user.getRoles()));
        return userDTO;
    }

    @NotNull
    public static List<UserDTO> toUserDTOList(@Nullable final List<User> users) {
        @NotNull List<UserDTO> userDTOList = new ArrayList<>();
        for (@Nullable User user : users) {
            userDTOList.add(toUserDTO(user));
        }
        return userDTOList;
    }

    @NotNull
    public static List<String> toStringRoles(@NotNull final List<Role> roles) {
        @NotNull List<String> userRoles = new ArrayList<>();
        for (@Nullable Role role : roles) {
            userRoles.add(role.toString());
        }
        return userRoles;
    }

    @Override
    public String toString() {
        return "[USER" + "';\n" +
                "  ID = '" + id + "';\n" +
                ", LOGIN = '" + login + "';\n" +
                ", PASSWORD HASH = '" + passwordHash + "';\n" +
                ", FIRST NAME = " + firstName + "';\n" +
                ", SECOND NAME = " + secondName + "';\n" +
                ", LAST NAME = " + lastName + "';]\n\n";
    }

}