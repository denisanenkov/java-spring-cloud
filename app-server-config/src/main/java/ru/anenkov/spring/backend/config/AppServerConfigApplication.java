package ru.anenkov.spring.backend.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * http://localhost:8888/app-test.properties
 * http://localhost:8888/app-server-api.properties
 * http://localhost:8888/app-eureka-server.properties
 * http://localhost:8888/app-server-enterprise.properties
 */

@EnableConfigServer
@SpringBootApplication
public class AppServerConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppServerConfigApplication.class, args);
    }

}
