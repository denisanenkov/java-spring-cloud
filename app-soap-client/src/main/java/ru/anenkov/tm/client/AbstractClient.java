package ru.anenkov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Component
public class AbstractClient {

    private final String cookie = "Cookie";

    private final String setCookie = "Set-Cookie";

    @Nullable
    private List<String> cookieHeaders = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Nullable
    private Map<String, Object> getHttpHeaders(@Nullable final Object port) {
        @Nullable final Map<String, Object> requestContext = getContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    @Nullable
    private Map<String, Object> getContext(@Nullable final Object port) {
        if (port == null) return null;
        @Nullable BindingProvider bindingProvider = (BindingProvider) port;
        return bindingProvider.getRequestContext();
    }

    @SuppressWarnings("unchecked")
    public void saveCookieHeaders(@Nullable final Object port) {
        if (port == null) return;
        @Nullable BindingProvider bindingProvider = (BindingProvider) port;
        final Map<String, Object> responseContext = bindingProvider.getResponseContext();
        final Map<String, Object> httpResponseHeaders = (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
        cookieHeaders = (List<String>) httpResponseHeaders.get(setCookie);
    }

    public void setListCookieRowRequest(@Nullable final Object port) {
        if (getContext(port) != null && getHttpHeaders(port) == null) {
            getContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        }
        @Nullable final Map<String, Object> httpRequestHeaders = getHttpHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(cookie, cookieHeaders);
    }

}
