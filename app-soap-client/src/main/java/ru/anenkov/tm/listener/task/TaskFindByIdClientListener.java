package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.ProjectDTO;
import ru.anenkov.spring.backend.endpoint.soap.ProjectEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.TaskEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;

@Component
public class TaskFindByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-task-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Show task by id";
    }

    @Async
    @Override
    @EventListener(condition = "@taskFindByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(projectEndpoint);
        System.out.print("ENTER NAME OF PROJECT TO GET TASK: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO;
        try {
            projectDTO = projectEndpoint.findProjectByUserIdAndName
                    (name);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
            return;
        }
        System.out.print("ENTER ID OF TASK: ");
        String id = TerminalUtil.nextLine();
        abstractClient.setListCookieRowRequest(taskEndpoint);
        try {
            taskEndpoint.findTaskByUserIdAndProjectIdAndId
                    (projectDTO.getId(), id);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND");
            return;
        }
    }

}
