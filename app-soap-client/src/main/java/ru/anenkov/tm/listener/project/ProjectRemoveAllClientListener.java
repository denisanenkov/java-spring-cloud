package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.ProjectEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.TaskEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectRemoveAllClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Projects-clear";
    }

    @Override
    public @Nullable String description() {
        return "Remove project by name";
    }

    @Async
    @Override
    @EventListener(condition = "@projectRemoveAllClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(projectEndpoint);
        System.out.print("[DELETE ALL PROJECTS]");
        projectEndpoint.removeAllProjects();
        System.out.println("[OK]");
    }

}