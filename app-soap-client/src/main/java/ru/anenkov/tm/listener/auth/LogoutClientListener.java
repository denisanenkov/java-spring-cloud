package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class LogoutClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @Override
    public @Nullable String arg() {
        return "-lout";
    }

    @Override
    public @Nullable String command() {
        return "Logout";
    }

    @Override
    public @Nullable String description() {
        return "Logout from system";
    }

    @Async
    @Override
    @EventListener(condition = "@logoutClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.print("LOGOUT..");
        authEndpoint.logout();
        abstractClient.setListCookieRowRequest(null);
        System.out.println("[OK]");
    }

}
