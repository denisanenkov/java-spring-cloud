package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class LoginClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @Override
    public @Nullable String arg() {
        return "-log";
    }

    @Override
    public @Nullable String command() {
        return "Login";
    }

    @Override
    public @Nullable String description() {
        return "Login user";
    }

    @Async
    @Override
    @EventListener(condition = "@loginClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.print("LOGIN: ");
        String login = TerminalUtil.nextLine();
        System.out.print("PASSWORD: ");
        String password = TerminalUtil.nextLine();
        authEndpoint.login(login, password);
        if (authEndpoint.login(login, password)) {
            System.out.println("LOGIN SUCCESS");
        } else System.out.println("LOGIN FAIL");
        abstractClient.saveCookieHeaders(authEndpoint);
        System.out.println("[OK]");
    }

}
