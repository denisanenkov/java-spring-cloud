package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.ProjectDTO;
import ru.anenkov.spring.backend.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectCreateClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-create";
    }

    @Override
    public @Nullable String description() {
        return "Create project";
    }

    @Async
    @Override
    @EventListener(condition = "@projectCreateClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(projectEndpoint);
        System.out.print("ENTER NAME: ");
        String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        String description = TerminalUtil.nextLine();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        long beginCount = projectEndpoint.count();
        projectEndpoint.addProject(projectDTO);
        long endCount = projectEndpoint.count();
        if (endCount - beginCount == 1) System.out.println("ADD PROJECT SUCCESS");
    }

}
