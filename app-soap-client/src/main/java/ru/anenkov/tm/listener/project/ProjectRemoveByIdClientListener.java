package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.spring.backend.endpoint.soap.ProjectEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class ProjectRemoveByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-remove-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Project remove by id";
    }

    @Async
    @Override
    @EventListener(condition = "@projectRemoveByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(projectEndpoint);
        System.out.print("ENTER ID OF PROJECT: ");
        String id = TerminalUtil.nextLine();
        projectEndpoint.removeOneById(id);
        System.out.println("\n[OK]");
    }

}
