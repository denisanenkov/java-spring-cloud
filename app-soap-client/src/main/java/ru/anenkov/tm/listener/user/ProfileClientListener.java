package ru.anenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

@Component
public class ProfileClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Profile";
    }

    @Override
    public @Nullable String description() {
        return "Show user profile";
    }

    @Async
    @Override
    @EventListener(condition = "@profileClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(authEndpoint);
        System.out.println("PROFILE: \n" + authEndpoint.profile().getId() + " \n" +
                authEndpoint.profile().getLogin() + " \n" +
                authEndpoint.profile().getPasswordHash() + " \n");
    }

}
