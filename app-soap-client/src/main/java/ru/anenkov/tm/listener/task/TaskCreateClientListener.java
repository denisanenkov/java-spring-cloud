package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.spring.backend.endpoint.soap.*;
import ru.anenkov.tm.client.AbstractClient;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.util.TerminalUtil;

import javax.xml.ws.soap.SOAPFaultException;

@Component
public class TaskCreateClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AbstractClient abstractClient;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private AuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-create";
    }

    @Override
    public @Nullable String description() {
        return "Create task";
    }

    @Async
    @Override
    @EventListener(condition = "@taskCreateClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        abstractClient.setListCookieRowRequest(taskEndpoint);
        System.out.print("ENTER NAME OF PROJECT TO ADD TASK: ");
        String name = TerminalUtil.nextLine();
        ProjectDTO projectDTO;
        try {
            projectDTO = projectEndpoint.findProjectByUserIdAndName
                    (name);
        } catch (SOAPFaultException ex) {
            System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
            return;
        }
        ru.anenkov.spring.backend.endpoint.soap.TaskDTO task = new TaskDTO();
        abstractClient.setListCookieRowRequest(authEndpoint);
        task.setProjectId(projectDTO.getId());
        task.setUserId(authEndpoint.profile().getId());
        System.out.print("ENTER NAME: ");
        String nameTask = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        String descriptionTask = TerminalUtil.nextLine();
        task.setName(nameTask);
        task.setDescription(descriptionTask);
        abstractClient.setListCookieRowRequest(projectEndpoint);
        long beginCount = taskEndpoint.countByUserIdAndProjectId(projectDTO.getId());
        taskEndpoint.addDTOTask(task);
        if (taskEndpoint.countByUserIdAndProjectId(projectDTO.getId()) - beginCount == 1) {
            System.out.println("ADDING SUCCESSFUL");
        } else {
            System.out.println("ADD TASK FAIL");
        }
    }

}
