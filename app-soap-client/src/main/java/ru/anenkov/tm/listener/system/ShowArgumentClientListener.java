package ru.anenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

import java.util.Collection;

@Component
public class ShowArgumentClientListener extends AbstractListenerClient {

    @Override
    public @Nullable String arg() {
        return "-a";
    }

    @Override
    public @Nullable String command() {
        return "Arguments";
    }

    @Override
    public @Nullable String description() {
        return "View arguments";
    }

    @Async
    @Override
    @EventListener(condition = "@showArgumentClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[ARGUMENTS]");
        @Nullable Collection<AbstractListenerClient> commandClients = bootstrap.commands.values();
        int index = 0;
        for (@Nullable AbstractListenerClient abstractCommandClient : commandClients) {
            String argument = abstractCommandClient.arg();
            if (argument != null) {
                index++;
                System.out.println(index + ".\t" + abstractCommandClient.arg());
            }
        }
        if (index == 0) System.out.println("[ARGUMENT LIST IS EMPTY]");
        System.out.println("[SUCCESS]");
    }

}
