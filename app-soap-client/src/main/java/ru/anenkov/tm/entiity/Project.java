package ru.anenkov.tm.entiity;

import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description;

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public String toString() {
        return "\nPROJECT: \n" +
                "ID='" + id + '\'' +
                ";\nNAME='" + name + '\'' +
                ";\nDESCRIPTION='" + description +
                ";\nDATE BEGIN ='" + dateBegin + '\'' +
                ";\nDATE END ='" + dateFinish + '\'' + "'\n";
    }

}
