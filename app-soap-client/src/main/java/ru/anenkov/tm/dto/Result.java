package ru.anenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    private Boolean success = true;

    public String str = "";

    public Result(Boolean success) {
        this.success = success;
    }

    public Result(Exception e) {
        success = false;
    }

}
