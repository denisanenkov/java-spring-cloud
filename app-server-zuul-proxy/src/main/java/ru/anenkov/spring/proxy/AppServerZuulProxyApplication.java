package ru.anenkov.spring.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class AppServerZuulProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppServerZuulProxyApplication.class, args);
    }

}
