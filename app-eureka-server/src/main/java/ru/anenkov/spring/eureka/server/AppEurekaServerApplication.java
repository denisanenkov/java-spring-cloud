package ru.anenkov.spring.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * dashboard: http://localhost:8761/
 */

@EnableEurekaServer
@SpringBootApplication
public class AppEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppEurekaServerApplication.class, args);
    }

}
